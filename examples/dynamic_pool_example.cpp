#include <memory>
#include <thread>
#include <chrono>
#include <iostream>

#include <uberpool/dynamic_pool.hpp>
#include <uberpool/state.hpp>

int main(int argc, char **argv) {

    auto state = std::make_shared<uberpool::State>("shared_pool", uberpool::Type::Core);
    uberpool::DynamicPool pool{state, 2};

    pool.enqueue(2, [](){
            const auto then = std::chrono::steady_clock::now();
            int i = 0;
            while(then + std::chrono::seconds{5} > std::chrono::steady_clock::now()) {
                i++;
            }
            return i;
        });
    pool.enqueue(2, [](){
            const auto then = std::chrono::steady_clock::now();
            int i = 0;
            while(then + std::chrono::seconds{10} > std::chrono::steady_clock::now()) {
                i++;
            }
            return i;
        });
    pool.enqueue(1, [](){std::this_thread::sleep_for(std::chrono::seconds{2}); std::cout << "Hello slowest" << std::endl;});
    pool.enqueue(2, [](){std::this_thread::sleep_for(std::chrono::seconds{1}); std::cout << "Hello slow" << std::endl;});
    pool.enqueue(1, [](){std::cout << "Hello World" << std::endl;});
    pool.wait();

    return 0;
}

