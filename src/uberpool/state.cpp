#include <uberpool/state.hpp>
#include <uberpool/enum_helper.hpp>
#include <thread>
#include <iostream>

namespace uberpool {

State::State(const std::string &shared_name, const Type pin_to_type) :
    name{shared_name},
    type{pin_to_type},
    cpusets{std::thread::hardware_concurrency()},
    segment{boost::interprocess::open_or_create, shared_name.c_str(), 65536},
    shared{*segment.find_or_construct<SharedState::Ptr>("state ptr")(
            segment.find_or_construct<SharedState>("state")(segment.get_segment_manager(), std::thread::hardware_concurrency()),      //object to own
            SharedState::VoidAllocator{segment.get_segment_manager()},  //allocator
            SharedState::Deleter{segment.get_segment_manager()}         //deleter
            )},
    state{shared} {

    if (hwloc_topology_init(&topology) < 0 || hwloc_topology_load(topology) < 0) {
        throw std::runtime_error("Unable to get hardware topology");
    }

    for (auto iter = hwloc_get_next_obj_by_type(topology, asHwlocType(pin_to_type), nullptr); iter != nullptr; iter = hwloc_get_next_obj_by_type(topology, HWLOC_OBJ_CORE, iter)) {
        for (
                auto jter = hwloc_get_next_obj_inside_cpuset_by_type(topology, iter->cpuset, HWLOC_OBJ_PU, nullptr);
                jter != nullptr;
                jter = hwloc_get_next_obj_inside_cpuset_by_type(topology, iter->cpuset, HWLOC_OBJ_PU, jter)) {
            cpusets[jter->os_index] = iter->cpuset;
        }
    }

    opportunity_cost.reserve(std::thread::hardware_concurrency());
    for (const auto &cpuset : cpusets) {
        std::vector<int> cost;
        cost.reserve(cpusets.size());
        for (unsigned int i = 0; i < cpusets.size(); ++i) {
            cost.emplace_back(hwloc_bitmap_isset(cpuset, i) == 0 ? 0 : 1);
        }
        opportunity_cost.emplace_back(cost);
    }
}

State::~State() {
    state.reset();
    if (shared.unique()) {
        segment.destroy_ptr(&shared);
        boost::interprocess::shared_memory_object::remove(name.c_str());
    }

    hwloc_topology_destroy(topology);
}

void State::set_this_thread_affinity(const Token &token) const {
    set_thread_affinity(token, pthread_self());
}

void State::set_thread_affinity(const Token &token, const pthread_t tid) const {
    if (hwloc_set_thread_cpubind(topology, tid, token.get_cpuset(), HWLOC_CPUBIND_STRICT) == -1) {
        std::cout << "BIND FAILED" << std::endl;
    }
}

void State::reset_this_thread_affinity() const {
    reset_thread_affinity(pthread_self());
}

void State::reset_thread_affinity(const pthread_t tid) const {
    hwloc_set_thread_cpubind(topology, tid, hwloc_get_root_obj(topology)->cpuset, 0);
}

Token State::claim(const int price) const {
    return state->claim(price, opportunity_cost, cpusets);
}

} // namespace uberpool
