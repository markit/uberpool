#pragma once

#include <uberpool/token.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/smart_ptr/shared_ptr.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <vector>

namespace uberpool {

/**
 * Stored in shared memory, this class is used to share the allocation of threads to process units.
 */
class SharedState {
    public:
        using SegmentManager = boost::interprocess::managed_shared_memory::segment_manager;
        using VoidAllocator = boost::interprocess::allocator<void, SegmentManager>;
        using Deleter = boost::interprocess::deleter<SharedState, SegmentManager>;
        using Ptr = boost::interprocess::shared_ptr<SharedState, VoidAllocator, Deleter>;

        SharedState(SegmentManager *const segment_manager, const size_t units);

        SharedState(const SharedState&) = delete;
        SharedState& operator=(const SharedState&) = delete;
        SharedState(SharedState&&) noexcept = delete;
        SharedState& operator=(SharedState&&) noexcept = delete;
        ~SharedState() = default;

        Token claim(const int price, const std::vector<std::vector<int>> &opportunity_cost, const std::vector<hwloc_cpuset_t> &cpusets);

        void release(const std::vector<int> &cost);

    private:
        using Mutex = boost::interprocess::interprocess_mutex;
        using ShmemAllocator = boost::interprocess::allocator<int, SegmentManager>;

        mutable Mutex counters_mutex;
        boost::interprocess::vector<int, ShmemAllocator> counters;
};

} // namespace uberpool
