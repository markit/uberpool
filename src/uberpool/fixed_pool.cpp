#include <uberpool/fixed_pool.hpp>

namespace uberpool {

void FixedPool::wait() {
    pool.wait();
}

FixedPool::FixedPool(std::shared_ptr<State> pool_state, const int relative_cost, const unsigned int size) :
    pool{std::move(pool_state)},
    cost{relative_cost} {

        const auto thread_count = (size > 0) ? size : std::thread::hardware_concurrency();
        pool.fill_pool(thread_count, [this]() {
            auto token = pool.pin_thread(cost);
            while (!pool.stop) {
                auto task = pool.get_next_task();
                if (task) {
                    task.value()();
                    pool.work_done.notify_all();
                }
            }
        });
}

} // namespace uberpool
