#pragma once

#include <uberpool/state.hpp>
#include <uberpool/token.hpp>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <optional>
#include <thread>
#include <queue>
#include <vector>

namespace uberpool {

/**
 * Helper class with functionality common to all pool types.
 */
template<class T>
class Pool {
    public:
        /**
         * Constructor
         */
        Pool(std::shared_ptr<State> pool_state);

        /**
         * Destructor, waits for pool to be stopped, but not required to be empty of work.
         */
        ~Pool();

        /**
         * Wait for pool to be idle.
         */
        void wait();

        /**
         * Fill the pool with threads to run.
         *
         * @param size The number of threads.
         * @param thread_func The function to run inside each thread.
         */
        template<class Lambda>
        void fill_pool(const size_t size, Lambda thread_func);

        /**
         * Pins a thread to a core.
         *
         * @param price The estimated price of this unit of work. Used to discourage other threads
         * from using the same core.
         * @returns The token representing the pinning.
         */
        [[nodiscard]]
        Token pin_thread(const int price);

        /**
         * Pop a task off of the work queue.
         *
         * @returns A task off of the backlog.
         */
        [[nodiscard]]
        std::optional<T> get_next_task();

        std::shared_ptr<State> state;

        std::mutex work_mutex;
        std::condition_variable work_available;
        std::condition_variable work_done;

        bool stop;
        std::vector<std::thread> workers;
        std::queue<T> work_queue;
};

template<class T>
Pool<T>::Pool(std::shared_ptr<State> pool_state) :
    state{std::move(pool_state)} {}

template<class T>
Pool<T>::~Pool() {
    stop = true;
    work_available.notify_all();

    for (auto &worker : workers) {
        worker.join();
    }
}

template<class T>
void Pool<T>::wait() {
    std::unique_lock<std::mutex> lock{work_mutex};
    if (!work_queue.empty()) {
        work_done.wait(lock, [this](){return work_queue.empty();});
    }
}

template<class T>
std::optional<T> Pool<T>::get_next_task() {
    std::unique_lock<std::mutex> lock{work_mutex};
    work_available.wait(lock, [this]() {return stop || !work_queue.empty();});
    if (stop && work_queue.empty()) {
        return {};
    }

    auto task = work_queue.front();
    work_queue.pop();
    return task;
}

template<class T>
template<class Lambda>
void Pool<T>::fill_pool(const size_t size, Lambda thread_func) {
    for (size_t i = 0; i < size; ++i) {
        workers.emplace_back(thread_func);
    }
}

template<class T>
Token Pool<T>::pin_thread(const int price) {
    auto result = state->claim(price);
    state->set_this_thread_affinity(result);
    return result;
}

}
