#pragma once

namespace uberpool {

/**
 * Enum for specifying the type of device.
 *
 * This is used for grouping processing units when deciding where to allocate a token.
 */
enum class Type {
    Core,
    ProcessingUnit,
};

} // namespace uberpool
