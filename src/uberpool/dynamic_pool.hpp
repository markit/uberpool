#pragma once

#include <uberpool/pool.hpp>
#include <future>
#include <tuple>

namespace uberpool {

/**
 * DynamicPool is a threadpool which allows tasks to be enqueued with different costs.
 * The underlying thread which picks up a task will then be allocated the least loaded resource at
 * that time based on the cost, and it is freed when the task finishes.
 */
class DynamicPool {
    public:
        /**
         * Constructor
         *
         * @param pool_state The shared state handler.
         * @param size The number of threads to spawn (0=number of cores).
         */
        DynamicPool(std::shared_ptr<State> pool_state, const unsigned int size = 0);

        /**
         * Enqueue a task.
         *
         * @param cost The cost the task represents.
         * @param f The Function representing the task.
         * @param args The arguments of the Function.
         * @returns A future which will point to the result of the function, once called.
         */
        template<class Function, class... Args>
        auto enqueue(const int cost, Function &&f, Args&&... args) -> std::future<decltype(f(args...))>;

        /**
         * @see Pool::wait
         */
        void wait();

    private:
        Pool<std::tuple<int, std::function<void()>>> pool;
};

template<class Function, class... Args>
auto DynamicPool::enqueue(const int cost, Function &&f, Args&&... args) -> std::future<decltype(f(args...))> {
    using T = decltype(f(args...));

    auto task = std::make_shared<std::packaged_task<T()>>(std::bind(std::forward<Function>(f), std::forward<Args>(args)...));
    auto result = task->get_future();
    {
        std::unique_lock<std::mutex> lock{pool.work_mutex};
        pool.work_queue.emplace(std::make_tuple(cost, [task](){(*task)();}));
    }
    pool.work_available.notify_one();
    return result;
}

} // namespace uberpool
