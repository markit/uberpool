#include <uberpool/dynamic_pool.hpp>

namespace uberpool {

DynamicPool::DynamicPool(std::shared_ptr<State> pool_state, const unsigned int size) :
    pool{std::move(pool_state)} {

        const auto thread_count = (size > 0) ? size : std::thread::hardware_concurrency();
        pool.fill_pool(thread_count, [this]() {
            while(!pool.stop) {
                auto task = pool.get_next_task();
                if (task) {
                    auto token = pool.pin_thread(std::get<0>(task.value()));
                    std::get<1>(task.value())();
                    pool.state->reset_this_thread_affinity();
                    pool.work_done.notify_all();
                }
            }
        });
}

void DynamicPool::wait() {
    pool.wait();
}

} // namespace uberpool
