#pragma once

#include <uberpool/enum.hpp>
#include <hwloc.h>

namespace uberpool {

/**
 * Helper function to convert uberpool::Type to hwloc type.
 */
[[nodiscard]]
hwloc_obj_type_t constexpr asHwlocType(const Type &type) noexcept {
    switch (type) {
        case Type::Core:
            return HWLOC_OBJ_CORE;
        case Type::ProcessingUnit:
            return HWLOC_OBJ_PU;
        default:
            return HWLOC_OBJ_SYSTEM;
    }
}

} // namespace uberpool
