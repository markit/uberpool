#pragma once

#include <uberpool/pool.hpp>
#include <uberpool/state.hpp>
#include <future>
#include <memory>

namespace uberpool {

/**
 * Fixed pool is a fixed cost threadpool which enqueues all tasks with the same cost.
 * Threads do not free resources (they stay pinned to the cores) when there are no tasks to
 * complete.
 */
class FixedPool {
    public:
        /**
         * Constructor
         *
         * @param pool_state The shared state handler.
         * @param relative_cost The cost of each thread to the system.
         * @param size The number of threads to spawn (0=number of cores).
         */
        FixedPool(std::shared_ptr<State> pool_state, const int relative_cost, const unsigned int size = 0);

        /**
         * Enqueue a task.
         *
         * @param f The Function representing the task.
         * @param args The arguments of the Function.
         * @returns A future which will point to the result of the function, once called.
         */
        template<class Function, class... Args>
        auto enqueue(Function &&f, Args&&... args) -> std::future<decltype(f(args...))>;

        /**
         * @see Pool::wait
         */
        void wait();

    private:
        Pool<std::function<void()>> pool;

        /// The cost for each thread in the pool.
        const int cost;
};

template<class Function, class... Args>
auto FixedPool::enqueue(Function &&f, Args&&... args) -> std::future<decltype(f(args...))> {
    using T = decltype(f(args...));

    auto task = std::make_shared<std::packaged_task<T()>>(std::bind(std::forward<Function>(f), std::forward<Args>(args)...));
    auto result = task->get_future();
    {
        std::unique_lock<std::mutex> lock{pool.work_mutex};
        pool.work_queue.emplace([task](){(*task)();});
    }
    pool.work_available.notify_one();
    return result;
}

} // namespace uberpool
