#include <uberpool/token.hpp>
#include <uberpool/shared_state.hpp>

namespace uberpool {

Token::Token(std::vector<int> &&cost_, const hwloc_cpuset_t cpuset_, SharedState *const parent_) :
    cost{std::move(cost_)},
    cpuset{cpuset_},
    parent{parent_} {
}

Token::~Token() {
    parent->release(cost);
}

hwloc_cpuset_t Token::get_cpuset() const {
    return cpuset;
}

} // namespace uberpool
