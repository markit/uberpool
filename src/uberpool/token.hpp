#pragma once

#include <vector>
#include <hwloc.h>

namespace uberpool {

class SharedState;

/**
 * A token represents allocated, not utilised resource.
 */
class Token {
    public:
        /**
         * Constructor
         *
         * @param cost_ The cost per ProcessingUnit.
         * @param cpuset_ The allocated ProcesingUnits.
         * @param parent_ A reference to the parent for cleanup purposes.:w
         */
        Token(std::vector<int> &&cost_, const hwloc_cpuset_t cpuset_, SharedState *const parent_);
        /**
         * Frees the resource via a pointer to SharedState.
         */
        ~Token();

        /**
         * Get the set of ProcessingUnits this token covers.
         */
        hwloc_cpuset_t get_cpuset() const;

    private:
        const std::vector<int> cost;
        const hwloc_cpuset_t cpuset;
        SharedState *const parent;
};

} // namespace uberpool
