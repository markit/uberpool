#pragma once

#include <uberpool/enum.hpp>
#include <uberpool/shared_state.hpp>
#include <uberpool/token.hpp>
#include <hwloc.h>
#include <string>

namespace uberpool {

/**
 * Connects to the underlying shared state and also queries the hardware topology using hwloc.
 */
class State {
    public:
        /**
         * Constructor, will create the underlying shared state if required, or attach.
         *
         * The grouping does not need to be consistent between instances of state accross processes.
         * For example, one can group by cores, but another, only exploiting memory locality, may
         * choose socket.
         *
         * @param shared_name The name of the shmem to connect/create.
         * @param pin_to_type The type of device to group by.
         */
        State(const std::string &shared_name, const Type pin_to_type);

        /**
         * Destructor,  will clean up shared state if last known user.
         *
         * @note If process crash use count becomes invalid.
         */
        ~State();

        /**
         * Claim a token.
         *
         * This represents reserving (non exclusively) a portion of the hardware amongst the other
         * threads using this library.
         *
         * @see SharedState::claim
         * @param price The price of this token, used to compare tokens.
         * @returns A Token assigned to the best device of pin_to_type at the time.
         */
        Token claim(const int price) const;

        /**
         * Pins the current thread according to the token.
         *
         * @see set_thread_affinity
         * @param token The token containing the cpu set to pin the thread to.
         */
        void set_this_thread_affinity(const Token &token) const;

        /**
         * Pins to the specified thread according to the provided token.
         *
         * @param token The token containing the cpu set to pin the thread to.
         * @param tid The thread id.
         */
        void set_thread_affinity(const Token &token, const pthread_t tid) const;

        /**
         * Reset the thread to be able to run on any processing unit.
         *
         * @see reset_thread_affinity
         */
        void reset_this_thread_affinity() const;

        /**
         * Reset the thread to be able to run on any processing unit.
         *
         * @param tid The thread id.
         */
        void reset_thread_affinity(const pthread_t tid) const;

    private:
        /// Name of the shared memory resource
        const std::string name;
        /// Type of processing resource to group threads by for this instance of state.
        const Type type;
        hwloc_topology_t topology;
        std::vector<hwloc_cpuset_t> cpusets;
        std::vector<std::vector<int>> opportunity_cost;
        boost::interprocess::managed_shared_memory segment;
        SharedState::Ptr &shared;
        SharedState::Ptr state;
};

} // namespace uberpool
