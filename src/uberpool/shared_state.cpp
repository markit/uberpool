#include <uberpool/shared_state.hpp>
#include <boost/range/combine.hpp>
#include <mutex>

namespace uberpool {

SharedState::SharedState(SegmentManager *const segment_manager, const size_t units) :
    counters{units, 0, ShmemAllocator{segment_manager}} {
    
}

void SharedState::release(const std::vector<int> &cost) {
    std::lock_guard<Mutex> lock{counters_mutex};
    // TODO assert size

    auto iter = cost.begin();
    for(auto &c : counters) {
        c -= *iter;
        ++iter;
    }
}

Token SharedState::claim(const int price, const std::vector<std::vector<int>> &opportunity_cost, const std::vector<hwloc_cpuset_t> &cpusets) {
    std::lock_guard<boost::interprocess::interprocess_mutex> lock{counters_mutex};
    const auto min = std::min_element(counters.begin(), counters.end());
    const auto id = static_cast<size_t>(std::distance(counters.begin(), min));

    auto cost = opportunity_cost[id];
    for (auto &c : cost) {
        c*= price;
    }

    auto iter = cost.begin();
    for (auto &c : counters) {
        c+= *iter;
        ++iter;
    }

    return Token{std::move(cost), cpusets[id], this};
}

} // namespace uberpool
